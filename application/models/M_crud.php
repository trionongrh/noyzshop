<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_crud extends CI_Model {

	function __construct(){
		parent:: __construct();
	}

	public function getData($table, $where = "", $order=""){
		if($where != ""){
		  if($order != ""){
		    return $this->db->order_by($order["column"],$order["order"])->get_where($table,$where);
		  }else{
		    return $this->db->get_where($table,$where);
		  }
		}else{
		  if($order != ""){
		    return $this->db->order_by($order["column"],$order["order"])->get($table);
		  }else{
		    return $this->db->get($table);
		  }
		}
	}

	public function insertData($table, $data) {

		return $this->db->insert($table,$data);
		
	}

	public function insertDataReturnId($table, $data) {
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}

	function deleteData($where,$table){
		$this->db->where($where);
		return $this->db->delete($table);
	}

	public function UpdateData($table,$data,$where){
		return $this->db->update($table,$data,$where);
	}


	public function getDataJoin($id_user){
		$this->db->select('users.*, detail_users.fullname, detail_users.address, detail_users.phone');
		$this->db->from('users');
		$this->db->join('detail_users', 'detail_users.id_users=users.id_user');
		$this->db->where('id_user', $id_user);
		return $this->db->get()->row_array();
	}



}
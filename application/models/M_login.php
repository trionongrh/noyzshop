<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model {

	function __construct(){
		parent:: __construct();
	}


	public function findUserdata($data){
		$this->db->select('users.*, detail_users.fullname, detail_users.address, detail_users.phone, detail_users.image_url');
		$this->db->from('users');
		$this->db->join('detail_users', 'detail_users.id_users=users.id_user');
		$this->db->where('username', $data['username']);
		$this->db->where('password', $data['password']);
		return $this->db->get()->row_array();
	}



}
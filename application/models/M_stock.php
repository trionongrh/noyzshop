<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stock extends CI_Model {

	function __construct(){
		parent:: __construct();
	}
	public function finddataBarangAll(){
		$this->db->select('barang.*, kategori_barang.nama_kategori');
		$this->db->from('barang');
		$this->db->join('kategori_barang', 'kategori_barang.id_kategori_barang=barang.id_kategori_barang');
		$this->db->where('barang.quantity_barang >', '0');
		return $this->db->get()->result_array();
	}

	public function finddataBarang($id){
		$this->db->select('barang.*, kategori_barang.nama_kategori');
		$this->db->from('barang');
		$this->db->join('kategori_barang', 'kategori_barang.id_kategori_barang=barang.id_kategori_barang');
		$this->db->where('id_barang',$id);
		return $this->db->get()->row_array();
	}

	function deleteDataBarang($table,$where){
		$this->db->where($where);
		return $this->db->delete($table);
	}

}
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title Page-->
    <title>Admin | Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url('assets/ca/css/font-face.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/font-awesome-4.7/css/font-awesome.min.css');?>" rel="stylesheet" media="all">


    <link href="<?php echo base_url('assets/ca/vendor/font-awesome-5.5/css/all.min.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/mdi-font/css/material-design-iconic-font.min.css');?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url('assets/ca/vendor/bootstrap-4.1/bootstrap.min.css');?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url('assets/ca/vendor/animsition/animsition.min.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/wow/animate.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/css-hamburgers/hamburgers.min.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/slick/slick.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/select2/select2.min.css');?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/ca/vendor/perfect-scrollbar/perfect-scrollbar.css');?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url('assets/ca/css/theme.css');?>" rel="stylesheet" media="all">

    <!-- DataTable CSS -->
    <link href="<?php echo base_url('assets/ca/vendor/datatable/css/dataTables.bootstrap4.min.css');?>" rel="stylesheet" media="all">

</head>

<body class="animsition">

    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="#">
                            <img src="<?php echo base_url('assets/ca/images/icon/logo.png');?>" alt="CoolAdmin" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="<?php base_url('admin/dashboard');?>">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="index.html">Dashboard 1</a>
                                </li>
                                <li>
                                    <a href="index2.html">Dashboard 2</a>
                                </li>
                                <li>
                                    <a href="index3.html">Dashboard 3</a>
                                </li>
                                <li>
                                    <a href="index4.html">Dashboard 4</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="chart.html">
                                <i class="fas fa-chart-bar"></i>Charts</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-table"></i>Tables</a>
                        </li>
                        <li>
                            <a href="form.html">
                                <i class="far fa-check-square"></i>Forms</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-calendar-alt"></i>Calendar</a>
                        </li>
                        <li>
                            <a href="map.html">
                                <i class="fas fa-map-marker-alt"></i>Maps</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-copy"></i>Pages</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="login.html">Login</a>
                                </li>
                                <li>
                                    <a href="register.html">Register</a>
                                </li>
                                <li>
                                    <a href="forget-pass.html">Forget Password</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>UI Elements</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="button.html">Button</a>
                                </li>
                                <li>
                                    <a href="badge.html">Badges</a>
                                </li>
                                <li>
                                    <a href="tab.html">Tabs</a>
                                </li>
                                <li>
                                    <a href="card.html">Cards</a>
                                </li>
                                <li>
                                    <a href="alert.html">Alerts</a>
                                </li>
                                <li>
                                    <a href="progress-bar.html">Progress Bars</a>
                                </li>
                                <li>
                                    <a href="modal.html">Modals</a>
                                </li>
                                <li>
                                    <a href="switch.html">Switchs</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grids</a>
                                </li>
                                <li>
                                    <a href="fontawesome.html">Fontawesome Icon</a>
                                </li>
                                <li>
                                    <a href="typo.html">Typography</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="<?php base_url('admin/dashboard');?>">
                    <img src="<?php echo base_url('assets/ca/images/icon/logo.png'); ?>" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar1">
                <div class="account2">
                    <div class="image img-cir img-120">
                        <img src="<?php echo base_url($_SESSION['image_url']); ?>" alt="John Doe" />
                    </div>
                    <h4 class="name"><?php echo $_SESSION['fullname']; ?></h4>
                    <?php 
                        $status = "";
                        if($_SESSION['status'] == 'superadmin'){
                            $status = "Super Admin";
                        }
                    ?>
                    <small><p class="badge badge-success"><?php echo $status; ?></p></small>
                    <a href="<?php echo base_url('admin/logout');?>">Sign out</a>
                </div>
                <?php
                    $active = array(
                        'dashboard' => "",
                        'stock_barang' => "",
                        'user_admin' => ""
                    );
                    if($head_name == "Dashboard"){
                        $active = array(
                            'dashboard' => "active",
                            'stock_barang' => "",
                            'user_admin' => ""
                        );
                    }else if($head_name == "Stock Barang"){
                        $active = array(
                            'dashboard' => "",
                            'stock_barang' => "active",
                            'user_admin' => ""
                        );
                    }else if($head_name == "Admin Management"){
                        $active = array(
                            'dashboard' => "",
                            'stock_barang' => "active",
                            'user_admin' => ""
                        );
                    }

                ?>
                <nav class="navbar-sidebar2">
                    <ul class="list-unstyled navbar__list">
                        <li class="<?php echo $active['dashboard'];?>">
                            <a href="<?php echo base_url('admin/dashboard');?>">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fas fa-edit"></i>Pesanan</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow <?php if($active['stock_barang']=="active"){echo 'open';} ?>" href="#">
                                <i class="fas fa-box"></i>Toko
                                <span class="arrow <?php if($active['stock_barang']=="active"){echo 'up';} ?>">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: <?php if($active['stock_barang']=="active"){echo 'block';} ?>;">
                                <li class="<?php echo $active['stock_barang'];?>">
                                    <a href="<?php echo base_url('admin/stock');?>">
                                        <i class="fas fa-list-alt"></i>Stock Barang</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow <?php if($active['user_admin']=="active"){echo 'open';} ?>" href="#">
                                <i class="fas fa-box"></i>User Admin
                                <span class="arrow <?php if($active['user_admin']=="active"){echo 'up';} ?>">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list" style="display: <?php if($active['user_admin']=="active"){echo 'block';} ?>;">
                                <li class="<?php echo $active['user_admin'];?>">
                                    <a href="<?php echo base_url('admin/user');?>">
                                        <i class="fas fa-list-alt"></i>Manage Admin</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">

                            <div class="form-header">
                                <div class="au-breadcrumb-content">
                                    <div class="au-breadcrumb-left">
                                        <ul class="list-unstyled list-inline au-breadcrumb__list">
                                            <li class="list-inline-item active">
                                                <a href="#">Admin</a>
                                            </li>
                                            <li class="list-inline-item seprate">
                                                <span>/</span>
                                            </li>
                                            <li class="list-inline-item"><?php echo $head_name;?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
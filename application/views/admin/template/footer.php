            </div>
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url('assets/ca/vendor/jquery-3.3.1.min.js'); ?>"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url('assets/ca/vendor/bootstrap-4.1/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/bootstrap-4.1/bootstrap.min.js'); ?>"></script>

    <!-- DataTable JS -->
    <script src="<?php echo base_url('assets/ca/vendor/datatable/js/jquery.datatables.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/datatable/js/datatables.bootstrap4.min.js'); ?>"></script>

    <!-- Format Number -->
    <script src="<?php echo base_url('assets/ca/vendor/format-number/jquery.number.min.js'); ?>"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url('assets/ca/vendor/slick/slick.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/ca/vendor/wow/wow.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/animsition/animsition.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/ca/vendor/counter-up/jquery.waypoints.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/counter-up/jquery.counterup.min.js'); ?>">
    </script>
    <script src="<?php echo base_url('assets/ca/vendor/circle-progress/circle-progress.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/perfect-scrollbar/perfect-scrollbar.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/chartjs/Chart.bundle.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/ca/vendor/select2/select2.min.js'); ?>">
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url('assets/ca/js/main.js'); ?>"></script>

    <script type="text/javascript">
        var site_url = "<?php echo site_url(); ?>";
        var base_url = "<?php echo base_url(); ?>";
    </script>

    <!-- Custom Page JS-->
    <script src="<?php echo base_url('assets/ca/js/stock.js'); ?>"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
            $("#notif-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#notif-alert").slideUp(500);
            });
        });

        
    </script>

</body>

</html>
<!-- end document-->

                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        


                        <!-- Tampilkan Alert -->
                        <?php echo $this->session->flashdata('msg'); ?>
                        

                        <div class="row">
                            <div class="col-md-12">
                                    <h2 class="title-1">Stock Barang</h2>
                            </div>
                        </div>


                        <div class="row m-t-25">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <span class="float-right mt-1">
                                            <button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#tambah-barang-modal">
                                            <i class="fa fa-plus"></i>&nbsp; Tambah Barang</button>
                                        </span>
                                    </div>
                                    <div class="card-body">
                                        <table id="example" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Gambar</th>
                                                    <th>Nama Barang</th>
                                                    <th>Kategori Barang</th>
                                                    <th>Quantity</th>
                                                    <th>Harga Barang</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php 
                                            $num = 1;
                                            foreach ($barang as $i => $val) { ?>
                                                <tr>
                                                    <td style="text-align: right;"><?php echo $num++;?></td>
                                                    <td style="text-align: center;"><img class="img-radius img-40" src="<?php echo base_url('assets/store/items/'. $val['url_gambar_barang']); ?>" alt="gbr" ></td>
                                                    <td><?php echo $val['nama_barang']; ?></td>
                                                    <td><?php echo $val['nama_kategori']; ?></td>
                                                    <td style="text-align: right;"><?php echo $val['quantity_barang']; ?></td>
                                                    <td style="text-align: right;">Rp. <?php echo number_format($val['harga_barang']); ?></td>
                                                    <td>
                                                        <button data-id="<?php echo $val['id_barang']; ?>" data-toggle="modal" data-target="#edit-barang-modal" type="button" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
                                                        <button data-id="<?php echo $val['id_barang']; ?>" data-nama="<?php echo $val['nama_barang']; ?>" data-toggle="modal" data-target="#hapus-barang-modal" type="button" class="btn btn-dark btn-sm"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                            <!-- <tfoot>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Position</th>
                                                    <th>Office</th>
                                                    <th>Age</th>
                                                    <th>Start date</th>
                                                    <th>Salary</th>
                                                </tr>
                                            </tfoot> -->
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>

                <!-- modal medium -->
                <div class="modal fade" id="tambah-barang-modal" tabindex="-1" role="dialog" aria-labelledby="tambah-barang-modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <strong>Tambah Barang</strong>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="<?php echo base_url('admin/stock/tambah_barang'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="modal-body mx-30">
                                <div class="container-fluid">
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="browse-click" class="form-control-label">Upload Gambar</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="float-left mt-1"> 

                                                        <input id="browse-click" type="button" class="btn btn-outline-secondary " value="Browse for files">
                                                        <!-- Hide this from the users view with css display:none; -->
                                                        <input id="file-type" type="file" size="4" name="file-gambar" hidden="" accept="image/*">
                                                    </div>
                                                    <div class="float-right mt-2"> 
                                                        <span class="badge badge-secondary" id="file-name" width="320" class >no file choosen</span>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="text-center image img-radius">
                                                        <img id="imgUp" src="<?php echo base_url('assets/ca/images/empty-image.jpg'); ?>" width="100%" height="100%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="inputnamaBarang" class="form-control-label">Nama Barang</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="inputnamaBarang" name="inputnamaBarang" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="inputquantityBarang" class=" form-control-label">Jumlah Barang</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="number" id="inputquantityBarang" name="inputquantityBarang" class="form-control" value="0">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="inputhargaBarang" class=" form-control-label">Harga Barang</label>
                                        </div>
                                        <div class="col-12 col-md-5">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" id="inputhargaBarang" name="inputhargaBarang" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Select</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="inputkategoriBarang" id="inputkategoriBarang" class="form-control">
                                                <option>Pilih Kategori</option>
                                            <?php foreach ($kategori as $i => $val) { ?>
                                                <option value="<?php echo $val['id_kategori_barang'];?>"><?php echo $val['nama_kategori'];?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end modal medium -->


                <!-- modal medium -->
                <div class="modal fade" id="edit-barang-modal" tabindex="-1" role="dialog" aria-labelledby="edit-barang-modalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <strong>Edit Data Barang</strong>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="<?php echo base_url('admin/stock/edit_barang'); ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="formeditBarang">
                            <div class="modal-body mx-30">
                                <div class="container-fluid">
                                    <input type="hidden" name="ideditBarang" id="ideditBarang">
                                    <input type="hidden" name="namagambareditBarang" id="namagambareditBarang">
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="browse-click" class=" form-control-label">Upload Gambar</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="float-left mt-1"> 

                                                        <input id="browse_editgambar" type="button" class="btn btn-outline-secondary " value="Browse for files">
                                                        <!-- Hide this from the users view with css display:none; -->
                                                        <input id="file_editgambar" type="file" size="4" name="file_editgambar" hidden="" accept="image/*">
                                                    </div>
                                                    <div class="float-right mt-2"> 
                                                        <span class="badge badge-secondary" id="file-editname" width="320" class >no file choosen</span>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="text-center image img-radius">
                                                        <img id="imgUpEdit" src="<?php echo base_url('assets/ca/images/empty-image.jpg'); ?>" width="100%" height="100%">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="editnamaBarang" class="form-control-label">Nama Barang</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="editnamaBarang" name="editnamaBarang" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="editquantityBarang" class=" form-control-label">Jumlah Barang</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="number" id="editquantityBarang" name="editquantityBarang" class="form-control" value="0">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="edithargaBarang" class=" form-control-label">Harga Barang</label>
                                        </div>
                                        <div class="col-12 col-md-5">
                                            <div class="input-group">
                                                <span class="input-group-addon">Rp</span>
                                                <input type="text" id="edithargaBarang" name="edithargaBarang" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Select</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <select name="editkategoriBarang" id="editkategoriBarang" class="form-control">
                                                <option value="0">Pilih Kategori</option>
                                            <?php foreach ($kategori as $i => $val) { ?>
                                                <option value="<?php echo $val['id_kategori_barang'];?>"><?php echo $val['nama_kategori'];?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Confirm</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- end modal medium -->

                <!-- modal small -->
            <div class="modal fade" id="hapus-barang-modal" tabindex="-1" role="dialog" aria-labelledby="hapus-barang-modalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="hapus-barang-modalLabel">Hapus Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?php echo base_url('admin/stock/hapus_barang'); ?>" method="post" id="formhapusBarang">
                        <div class="modal-body">
                            <input type="hidden" name="idhapusBarang" id="idhapusBarang">
                            <p>
                                Apakah anda ingin menghapus barang <strong id="hapusnamaBarang">test</strong>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Confirm</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end modal small -->

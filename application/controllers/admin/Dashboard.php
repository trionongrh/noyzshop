<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {



	public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('username')==""){
        	redirect('admin/login');
        }
    }

	public function index()
	{
		$head['head_name'] = "Dashboard";
		$this->load->view('admin/template/header',$head);
		$this->load->view('admin/dashboard');
		$this->load->view('admin/template/footer');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('username')==""){
        	redirect('admin/login');
        }
        $this->load->model('m_stock');
    }

	public function index()
	{
		$data_barang['barang'] = $this->m_stock->finddataBarangAll();
		$data_barang['kategori'] = $this->m_crud->getData('kategori_barang')->result_array();

		$head['head_name'] = "Stock Barang";
		$this->load->view('admin/template/header',$head);
		$this->load->view('admin/stock',$data_barang);
		$this->load->view('admin/template/footer');
	}


	public function tambah_barang(){
		$this->upload_file();

		$harga = str_replace(",","",$this->input->post('inputhargaBarang'));

		//Upload File
		if ( ! $this->upload->do_upload('file-gambar'))
        {
                $error = array('error' => $this->upload->display_errors());
                $file_name="";
                $this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
											<span class="badge badge-pill badge-danger">Error</span>
											File gambar tidak boleh kosong
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>');
                redirect('admin/stock');
                exit();
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $file_name = $this->upload->data('file_name');
        }
        //construct data untuk database
		$data_barang = array(
			'nama_barang' => $this->input->post('inputnamaBarang'),
			'quantity_barang' => $this->input->post('inputquantityBarang'),
			'harga_barang' => $harga,
			'url_gambar_barang' => $file_name,
			'id_kategori_barang' => $this->input->post('inputkategoriBarang'),
			'tanggal_input_barang' => date('Y-m-d H:i:s')
		);

		//Input ke database
		if($this->m_crud->insertData('barang',$data_barang)){
			$this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-success alert-dismissible fade show">
											<span class="badge badge-pill badge-success">Success</span>
											Data telah berhasil di-input ke database
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>');


			redirect('admin/stock');
		} else {
			$this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
											<span class="badge badge-pill badge-danger">Error</span>
											Data gagal di-input ke database
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>');
			redirect('admin/stock');
		}

	}

	public function edit_barang(){
		$this->upload_file();
		$file_name = "";
		$barang = $this->input->post();
		$harga = str_replace(",","",$this->input->post('edithargaBarang'));
		$gambar_lama = $this->m_stock->finddataBarang($barang['ideditBarang'])['url_gambar_barang'];

		$pathgambar = './assets/store/items/'.$gambar_lama;

		if($gambar_lama==$barang['namagambareditBarang']){
			$file_name = $gambar_lama;
		}else{
			unlink($pathgambar);
			//Upload File
			if ( ! $this->upload->do_upload('file_editgambar'))
	        {
	                $error = array('error' => $this->upload->display_errors());
	                $file_name="";
	                $this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
												<span class="badge badge-pill badge-danger">Error</span>
												File gambar tidak boleh kosong
												<button type="button" class="close" data-dismiss="alert" aria-label="Close">
													<span aria-hidden="true">×</span>
												</button>
											</div>');
	                redirect('admin/stock');
	                exit();
	        }
	        else
	        {
	                $data = array('upload_data' => $this->upload->data());
	                $file_name = $this->upload->data('file_name');
	        }
		}
        //construct data untuk database
		$data_barang = array(
			'nama_barang' => $this->input->post('editnamaBarang'),
			'quantity_barang' => $this->input->post('editquantityBarang'),
			'harga_barang' => $harga,
			'url_gambar_barang' => $file_name,
			'id_kategori_barang' => $this->input->post('editkategoriBarang'),
			'tanggal_edit_barang' => date('Y-m-d H:i:s')
		);

		//Input ke database
		if($this->m_crud->UpdateData('barang',$data_barang,array('id_barang' => $barang['ideditBarang']))){
			$this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-success alert-dismissible fade show">
											<span class="badge badge-pill badge-success">Success</span>
											Data telah berhasil di-ubah ke database
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>');


			redirect('admin/stock');
		} else {
			$this->session->set_flashdata('msg','<div id="notif-alert" class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
											<span class="badge badge-pill badge-danger">Error</span>
											Data gagal di-ubah ke database
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">×</span>
											</button>
										</div>');
			redirect('admin/stock');
		}
	}

	public function hapus_barang(){

	}







	// Purpose function (AJAX, Construct, Config)


	function upload_file(){
        $config['upload_path']          = './assets/store/items/';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg|bmp';
        $config['max_size']             = 0;

        $this->load->library('upload', $config);
    }

    public function getallKategori(){
    	$kategori = $this->m_stock->getKategori();
    	echo json_encode($kategori);
    }

	public function find_barang($id_barang){
		$data = $this->m_stock->finddataBarang($id_barang);
		echo json_encode($data);
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {



	public function __construct()
    {
        parent::__construct();
        $this->load->model('m_login');
        if($this->session->userdata('username')!=""){
        	redirect('admin/dashboard');
        }
    }

	public function index()
	{
			$this->load->view('admin/login');
	}

	public function dologin(){
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
		);

		$logincheck = $this->m_login->findUserdata($data);

		if($logincheck!=NULL){
			$session_data = array(
				'username' => $logincheck['username'],
				'fullname' => $logincheck['fullname'],
				'email' => $logincheck['email'],
				'image_url' => $logincheck['image_url'],
				'status' => $logincheck['status']
			);
			$this->session->set_userdata($session_data);
			redirect('admin/dashboard');
		}else{
			echo 'gagal';
		}
	}
}

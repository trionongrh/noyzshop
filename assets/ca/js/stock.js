function inputgambarFn() {
    var split = $('#file-type').val().split('\\');
    $('#file-name').html(split[2]);
};

function editgambarFn() {
    var split = $('#file_editgambar').val().split('\\');
    $('#file-editname').html(split[2]);
    $('#namagambareditBarang').val(split[2]);
};


$('#browse-click').on('click', function () { // use .live() for older versions of jQuery
    $('#file-type').click();
    return false;
});

$('#browse_editgambar').on('click', function () { // use .live() for older versions of jQuery
    $('#file_editgambar').click();
    return false;
});

$('#file-type').on("change", function(){
    inputgambarFn();
    readURL('#imgUp',this);
});

$('#file_editgambar').on("change", function(){
    editgambarFn();
    readURL('#imgUpEdit',this);
});


function readURL(object,input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $(object).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


$('#tambah-barang-modal').on('hidden.bs.modal', function () {
    $('#inputquantityBarang').val(0);
    $('#inputnamaBarang').val('');
    $('#inputhargaBarang').val('');
    $('#imgUp').attr('src', base_url+'assets/ca/images/empty-image.jpg');
    $('#file-type').val('');
    $('#file-name').html('no file choosen');
})

$('#tambah-barang-modal').on('show.bs.modal', function () {
    $('#inputhargaBarang').number(true);
})


$('#edit-barang-modal').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).data('id');
    $('#ideditBarang').val(id);
    $.ajax({
        type: "GET",
        url: base_url+'admin/stock/find_barang/'+id,
        dataType : "json",
        success: function(result){
            console.log(result.id_kategori_barang);
            $('#imgUpEdit').attr('src', base_url+'assets/store/items/'+result.url_gambar_barang);
            $('#editnamaBarang').val(result.nama_barang);
            $('#editquantityBarang').val(result.quantity_barang);
            $('#edithargaBarang').val(result.harga_barang);
            $('#editkategoriBarang').val(result.id_kategori_barang);
            $('#edithargaBarang').number(true);
            $('#namagambareditBarang').val(result.url_gambar_barang);
        }
    })
})

$('#hapus-barang-modal').on('show.bs.modal', function (e) {
    var id = $(e.relatedTarget).data('id');
    var nama = $(e.relatedTarget).data('nama');
    $('#hapusnamaBarang').html(nama);
})